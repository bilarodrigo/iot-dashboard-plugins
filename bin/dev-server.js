#!/usr/bin/env node
'use strict';
var connect = require('connect');
var serveStatic = require('serve-static');
var path = require("path");
var dashboardDir = path.join(__dirname, "../node_modules/iot-dashboard/dist");
var pluginsDir = path.join(__dirname, "../dist");
connect()
    .use(serveStatic(pluginsDir))
    .use(serveStatic(dashboardDir))

    .listen(8081, function () {
        console.log('Serving ' + dashboardDir);
        console.log('Serving ' + pluginsDir);
        console.log('Server running on 8081 ...');
    });
